<?php
/**
 * Class Config
 */
class Config {
    /**
     * @return array
     */
    public static function getPageAliases(){
        return array(
            '/' => 'Home',
            '404' => '404 Not Found',
            'our-projects' => 'Our Projects',
            'about-us' => 'About Us',
            'contact-us' => 'Contact Us'
        );
    }

}