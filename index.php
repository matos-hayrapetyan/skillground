<?php
require 'load.php';

if( ! isset( $_GET['p'] ) ){
    $view = 'public/home.php';
    $page = '/';
}elseif( strstr( $_GET['p'], '.' ) ){
    $view = 'public/'.$_GET['p'];
    $page = $_GET['p'];
}else{
    $view = 'public/'.$_GET['p'].'/index.php';
    $page = $_GET['p'];
}

if( !file_exists( $view ) ){
    $view = 'public/404.php';
    $page = '404';
}
?>
<!DOCTYPE>
<html>
    <head>
        <title><?php echo Config::getPageAliases()[$page]; ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <meta charset="utf-8" />
    </head>
    <body>
        <div id="flex-page">
            <?php
            require 'public/header.php';
            require $view;
            require 'public/footer.php'
            ?>
        </div>
        <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link type="text/css" rel="stylesheet" href="vendor/materialize/css/materialize.min.css"  media="screen,projection"/>
        <link rel="stylesheet" href="assets/css/style.css" />
        <script type="text/javascript" src="vendor/jquery.min.js"></script>
        <script type="text/javascript" src="vendor/materialize/js/materialize.min.js"></script>
        <script src="assets/js/init.js"></script>
    </body>
</html>