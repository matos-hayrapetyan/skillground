<div class="container">
    <div class="section center">
        <p class="justify-text"><a href="/">SkillGround</a> a non-governmental organization (NGO), officially formed on January 1, 2017, has been continuing its activities beyond the borders of Armenia. Having the ability to attract people of various professions and concepts, organization brings together around one common idea: <strong>Available education, regardless of the physical and social status</strong></p>
    </div>
    <div class="section">
        <h3>Ideas</h3>
        <ul class="list-circle browser-default">
            <li>Justice</li>
            <li>Independence</li>
            <li>Humanity</li>
        </ul>
        <p class="justify-text">Seeking and, why not, creating an opportunity for the available education, the organization conducts free courses for representatives of socially vulnerable layers of the society, thus allowing everyone to be equal.</p>
    </div>
    <div class="section">
        <h3>Objectives</h3>
        <p class="justify-text">Company's main objective is to secure providing of education for socially disadvantaged and vulnerable participants. Eventually, not everyone can have the opportunity to receive a proper education. A key aim of the organization is to make the members of society equal through practical knowledge and capacity.</p>
    </div>
    <div class="section">
        <h3>Our History</h3>
        <p class="justify-text">Back in 2012, with the effort of the young people there was founded a club. This gives an opportunity to gather all the students who had some difficulties to understand or assimilate the teaching material, and obtain additional relevant curriculum exclusively free of charge.</p>
        <p class="justify-text">Being a startup and in the found stage and not having a main center, the club conducts classes at the university, without any obstacles. Gradually extending its circles, club members began to conduct courses by subject groups, based on the teaching of foreign languages and computer skills. Already in January 2015, in collaboration with various organizations and already having sponsors, club members have continued their activities in new opened center.</p>
        <p class="justify-text">During these years, the organization has implemented a <a href="/our-projects">number of programs</a> to make their classes more efficient and organized. Particularly, opening of a summer school camp, teaching staff training and the use of modern methods of teaching had a great importance for vulnerable children. Since 2017 the center has been reorganized and officially opened as a public organization purporting to carry out its activities beyond the borders of Armenia, the Armenian foreign communities.</p>
        <p class="justify-text">Over the years periodically there voiced the question of why it was chosen as "charity" in this way.You had sponsors and also partly financing by yourselves, why didn’t give the money, to make life better for children. We finally answered “It is true that today are the favorable social conditions are the basis of the prosperous future, and, no matter how it painful is, its absence immediately noticeable, but not always, that money helps and secure the future”.</p>
        <p class="justify-text">There are more important values for a lifetime, the principal of which is knowledge. And if many smart and talented children and teens cannot use that knowledge because of the lack of social favorable conditions, then we create the right conditions for them to learn, giving not only new knowledge but also an opportunity to gain the ability and skill.</p>
    </div>
</div>