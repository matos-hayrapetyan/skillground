<div class="flex justify-center">
    <h4 class="center orange-text">Nothing found here</h4>
    <p class="center">We could not find anything here.</p>
    <div class="row center">
        <a href="/" id="download-button" class="btn waves-effect waves-light blue">Home</a>
    </div>
</div>
